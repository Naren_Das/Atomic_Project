(function ($) {
    $('.banner, h1, h3, hr, h4').velocity({
        translateY: [0, 500],
        opacity: [1, 0]
    }, {
        duration: 1900,
        display: 'block',
        easing: [70, 8]
    }).velocity('transition:bounceUpIn', { delay: 1000});
})(jQuery);

