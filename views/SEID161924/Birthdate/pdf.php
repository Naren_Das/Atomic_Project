<?php
require_once('../../../vendor/autoload.php');
use App\Birthdate\Birthdate;
use App\Utility\Utility;

$objBirthdate = new Birthdate();
$allData = $objBirthdate->index();

$tableRows = "";
$sl = 0;

foreach ($allData as $row){
    $id = $row->id;
    $customerName = $row->customer_name;
    $birthDate = $row->birthdate;
    $sl++;
    $tableRows .= "<tr>";

    $tableRows .= "<td align='center' width='50'> $sl </td>";
    $tableRows .= "<td align='center' width='50'> $id </td>";
    $tableRows .= "<td align='center' width='250'> $customerName </td>";
    $tableRows .= "<td align='center' width='250'> $birthDate </td>";

    $tableRows .= "</tr>";
}

$html =<<<birthDate
    <head>
        <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/file.css">
    </head>

    <div class="container">
        <p>Customer BirthDay</p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Serial</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>ID</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Book Name</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Author Name</th>
                </tr>
            </thead>
            
            <tbody>
                $tableRows;
            </tbody>
            
        </table>
    </div> 
birthDate;

//third party class file:
require_once('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

//Output a PDF file directory to the browser: Download file:
$mpdf->Output('Customer BirthDate.pdf','D');


