<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBirthdate = new \App\Birthdate\Birthdate();
$objBirthdate->setData($_GET);
$objBirthdate->delete();

Utility::redirect($path);