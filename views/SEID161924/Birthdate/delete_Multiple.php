<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBirthdate = new \App\Birthdate\Birthdate();

$deleteItems = $_POST['multiple'];

foreach($deleteItems as $deleteId){
    $_GET['id'] = $deleteId;
    $objBirthdate->setData($_GET);
    $objBirthdate->delete();
}

Utility::redirect($path);