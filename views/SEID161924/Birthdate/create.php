<?php
require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
$msg = Message::message();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="../../../resources/css/style.css">
        <link rel="stylesheet" type="text/css" media="screen" href="../../../resources/css/jquery-ui-1.8.14.custom.css">
        <title>Birthdate</title>
    </head>
    <body>

        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->
        <div class="col-sm-12  container contentView">
            <div class="col-sm-4"></div>
            <div class="col-sm-4 content">
                <h3>Enter Your Birthdate</h3>
                <hr/>
                <div class="subContent">
                    <form action="../Birthdate/store.php" method="post">

                        <div class="form-group" style="display: block">
                            <label for="ID">Name: </label>
                            <input type="text" class="form-control" id="ID" name="customerName" placeholder="Enter Your Name..." required="required">
                        </div>

                        <div class="form-group col-md-16 ">
                            <label for="Name">Birthdate: </label>
                            <div class=" selectContainer">
                                <div class="input-group dateOpacity">
                                    <input type="text" id="date" name="birthDate" size="15" required="required"/>
                                </div>
                            </div>
                        </div>

                        <button type="submit" name="storeInfo" class="btn btn-default">Submit</button>
                        <button type="reset" class="btn btn-default">Refresh</button>
                    </form>


                    <hr/>
                    <a href="index.php" style="float: left"><button class="btn btn-default">View Data</button></a>
                </div>
            </div>
            <!--Messeage-->
            <?php
            echo"
               <div style='margin-top: 5%; margin-left: 40%; position: fixed; text-align: center; z-index: 2'>
                   <span style='background-color: #4cae4c; color: white; border-radius: 4px;' id='message'>$msg</span>
               </div>
               ";
            ?>


            <div class="col-sm-4"></div>
        </div>



    <!--Script Files-->
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery-1.5.1.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);

        $("#date").datepicker({ dateFormat: 'yy-mm-dd'});

        //Jquery for container div slide For this we have to add an id
        /*$(window).ready(function () {
         $("#contentDiv").hide();
         $("#contentDiv").slideDown(2000);
         });*/
    </script>
    </body>
</html>