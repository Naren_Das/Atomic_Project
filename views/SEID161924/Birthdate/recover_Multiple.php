<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBirthdate = new \App\Birthdate\Birthdate();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objBirthdate->setData($_GET);
    $objBirthdate->recover();
}

Utility::redirect('index.php');