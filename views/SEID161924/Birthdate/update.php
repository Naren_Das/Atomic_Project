<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBirtdate = new \App\Birthdate\Birthdate();
$objBirtdate->setData($_POST);
$objBirtdate->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}