<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBirthdate = new \App\Birthdate\Birthdate();

$objBirthdate->setData($_GET);

$objBirthdate->trash();

Utility::redirect('trashed.php');