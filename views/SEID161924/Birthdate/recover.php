<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBirthdate = new \App\Birthdate\Birthdate();

$objBirthdate->setData($_GET);

$objBirthdate->recover();

Utility::redirect('index.php');