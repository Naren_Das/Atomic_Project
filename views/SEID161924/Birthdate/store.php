<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

if(isset($_POST['storeInfo'])){
    $objBirthdate = new \App\Birthdate\Birthdate();
    $objBirthdate->setData($_POST);
    $objBirthdate->store();
}

Utility::redirect('create.php');