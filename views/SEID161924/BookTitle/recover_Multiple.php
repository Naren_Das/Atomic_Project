<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookTitle = new \App\BookTitle\BookTitle();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objBookTitle->setData($_GET);
    $objBookTitle->recover();
}

Utility::redirect('index.php');