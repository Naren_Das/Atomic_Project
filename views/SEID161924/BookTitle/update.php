<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookTitle = new \App\BookTitle\BookTitle();

$objBookTitle->setData($_POST);

$objBookTitle->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}

