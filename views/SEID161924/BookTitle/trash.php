<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookTitle = new \App\BookTitle\BookTitle();

$objBookTitle->setData($_GET);

$objBookTitle->trash();

Utility::redirect('trashed.php');