<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle->setData($_GET);
$objBookTitle->delete();

Utility::redirect($path);
