<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookTitle = new \App\BookTitle\BookTitle();

$trashItems = $_POST['multiple'];

foreach($trashItems as $trashId){
    $_GET['id'] = $trashId;
    $objBookTitle->setData($_GET);
    $objBookTitle->trash();
}

Utility::redirect('trashed.php');
