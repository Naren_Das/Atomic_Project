<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();
$objFavourite->setData($_GET);
$objFavourite->delete();

Utility::redirect($path);