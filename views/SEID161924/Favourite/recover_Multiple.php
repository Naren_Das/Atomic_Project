<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objFavourite->setData($_GET);
    $objFavourite->recover();
}

Utility::redirect('index.php');