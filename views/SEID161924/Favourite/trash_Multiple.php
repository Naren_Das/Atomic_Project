<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();

$trashItems = $_POST['multiple'];

foreach($trashItems as $trashId){
    $_GET['id'] = $trashId;
    $objFavourite->setData($_GET);
    $objFavourite->trash();
}

Utility::redirect('trashed.php');
