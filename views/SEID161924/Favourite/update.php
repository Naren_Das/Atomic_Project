<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();

$objFavourite->setData($_POST);

$objFavourite->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}
