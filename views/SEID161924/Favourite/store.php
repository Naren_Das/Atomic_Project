<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

if(isset($_POST['storeInfo'])){
    $objFavourite = new \App\Favourite\Favourite();
    $objFavourite->setData($_POST);
    $objFavourite->store();
}

Utility::redirect('create.php');