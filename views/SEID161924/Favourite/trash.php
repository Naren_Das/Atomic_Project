<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();

$objFavourite->setData($_GET);

$objFavourite->trash();

Utility::redirect('trashed.php');