<?php
require_once("../../../vendor/autoload.php");
if(!isset($_SESSION))session_start();

use App\Message\Message;
$msg = Message::message();

$objFavourite = new \App\Favourite\Favourite();

/**************************************************Active Item >> function(start)**************************************************/
$allData = $objFavourite->index();
/**************************************************Active Item >> function(end)**************************************************/

/**************************************************2.Search >> function(Start)************************************************/
if(isset($_REQUEST['search']) ){
    $someData =  $objFavourite->search($_REQUEST);
}

$availableKeywords = $objFavourite->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
/**************************************************2.Search >> function(end)**************************************************/

/**************************************************1. Pagination Block 1 >> start**************************************************/

$recordCount = count($allData);

//For page number:
if(isset($_REQUEST['Page'])){
    $page = $_REQUEST['Page'];
}
else if(isset($_REQUEST['Page'])){
    $page = $_REQUEST['Page'];
}
else{
    $page = 1;
}
$_SESSION['Page'] = $page;


//For Perpage item:
if(isset($_REQUEST['ItemPerPage'])){
    $itemPerPage = $_REQUEST['ItemPerPage'];
}
else if(isset($_SESSION['ItemPerPage'])){
    $itemPerPage = $_SESSION['ItemPerPage'];
}
else{
    $itemPerPage = 3;
}
$_SESSION['ItemPerPage'] = $itemPerPage;


$pagesRequired = ceil($recordCount / $itemPerPage);
$someData = $objFavourite->indexPaginator($page,$itemPerPage);
$allData = $someData;

$serial = (($page - 1) * $itemPerPage) + 1;

if($serial < 1){
    $serial = 1;
}
/**************************************************1. Pagination Block 1 >> end**************************************************/

/**************************************************4.Search >> Show Data (Start)**************************************************/
if(isset($_REQUEST['search']))
    $someData = $objFavourite->search($_REQUEST);
if(isset($_REQUEST['search'])){
    $allData = $someData;
}
/**************************************************4.Search >> Show Data (end)**************************************************/

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css">
        <link rel="stylesheet" type="text/css" href="../../../../Hotel_Reservation_System/resources/cssFile/jquery-ui.css">
        <title>Favourite List</title>
    </head>
    <body>

        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->

        <div class="col-sm-12 container contentView">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 content" style="z-index: 1">

                <!--*************************************************1.Search >> form (Start)***********************************************-->
                <div style="margin-left: 60%">
                    <form id="searchForm" action="index.php"  method="get" style="margin-top: 5px; margin-bottom: 10px">
                        <input class="form-control" type="text" value="" id="searchID" name="search" placeholder="Search here...">
                        <input type="checkbox"  name="byName"   checked >By Name
                        <input type="checkbox"  name="bySection"  checked >By Section
                        <input hidden type="submit" class="btn-primary" value="search">
                    </form>
                </div>
                <!--*************************************************1.Search >> form (end)*************************************************-->

                <h3>Active List: Favourite</h3>
                <hr/>

                <!--Table Design using Bootstrap-->
                <form id="selectionForm" action="trash_Multiple.php" method="post">
                <table class="table table-bordered data">
                    <tr>
                        <th>Select All <input type="checkbox" id="select_all" name="select_all"/></th>
                        <th>Serial</th>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Section</th>
                        <th>Action</th>
                    </tr>

                    <?php
                    foreach ($allData as $record){
                        echo "
                                    <tr>
                                        <td align='center'><input type='checkbox' class='checkbox' name='multiple[]' value='$record->id'/></td>
                                        <td>$serial</td>
                                        <td>$record->id</td>
                                        <td>$record->customer_name</td>
                                        <td>$record->book_list</td>
                                        <td>
                                            <a href='../Favourite/view.php?id=$record->id' class='btn btn-primary btn-sm'><span class='glyphicon glyphicon-eye-open'></span> View</a>
                                            <a href='../Favourite/edit.php?id=$record->id&mode=1' class='btn btn-primary btn-sm'><span class='glyphicon glyphicon-pencil'></span> Edit</a>
                                            <a href='../Favourite/trash.php?id=$record->id' class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-trash'> Trash</span></a>
                                            <a href='../Favourite/delete.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-remove-sign'> Delete</span> </a>
                                            <a href='../Favourite/email.php?id=$record->id' class='btn btn-primary btn-sm'><span class='glyphicon glyphicon-envelope'> Email</span></a>
                                        </td>
                                    <tr/>
                                ";
                        $serial++;
                    }
                    ?>

                    <tr>
                        <td colspan="6">
                            <a href="../Favourite/create.php"><input type="button" value="Add New" class="btn btn-success btn-sm"/></a>
                            <a href="../Favourite/trashed.php"><input type="button" value="Trash List" class="btn btn-warning btn-sm"/></a>
                            <input type="button" id="trashMultiple" value="Trash Selected" class="btn btn-warning btn-sm"/>
                            <input type="button" id="deleteMultiple" value="Delete Selected" class="btn btn-danger btn-sm"/>
                            <a href="pdf.php" class="btn btn-primary btn-sm">PDF</a>
                            <a href="xl.php" class="btn btn-primary btn-sm">Excel</a>
                            <a href="email.php?list=1" class="btn btn-primary btn-sm">Email Active List</a>
                        </td>
                    </tr>

                </table>
            </form>

                <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                <div align="left" class="container">

                    <ul class="pagination">

                        <?php

                        $pageMinusOne = $page - 1;
                        $pagePlusOne = $page + 1;

                        if($page > $pagesRequired){
                            Utility::redirect("index.php?Page=$pagesRequired");
                        }

                        if($page > 1){
                            echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                        }

                        for($i=1; $i<=$pagesRequired; $i++){
                            if($i == $page){
                                echo '<li class="active"><a href="">'. $i . '</a></li>';
                            }

                            else{
                                echo "<li><a href='?Page=$i'>". $i . '</a></li>';
                            }
                        }

                        if($page < $pagesRequired){
                            echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";
                        }
                        ?>


                        <select class="form-control" name="ItemPerPage" id="ItemPerPage" onchange="javascript:location.href = this.value;">
                            <?php

                            if($itemPerPage == 3){
                                echo "<option value='?ItemPerPage=3' selected>Show 3 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=3'>Show 3 Items Per Page</option>";
                            }

                            if($itemPerPage == 4){
                                echo "<option value='?ItemPerPage=4' selected>Show 4 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=4'>Show 4 Items Per Page</option>";
                            }

                            if($itemPerPage == 5){
                                echo "<option value='?ItemPerPage=5' selected>Show 5 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=5'>Show 5 Items Per Page</option>";
                            }

                            if($itemPerPage == 6){
                                echo "<option value='?ItemPerPage=6' selected>Show 6 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=6'>Show 6 Items Per Page</option>";
                            }

                            if($itemPerPage == 10){
                                echo "<option value='?ItemPerPage=10' selected>Show 10 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=10'>Show 10 Items Per Page</option>";
                            }

                            if($itemPerPage == 15){
                                echo "<option value='?ItemPerPage=15' selected>Show 15 Items Per Page</option>";
                            }
                            else{
                                echo "<option value='?ItemPerPage=15'>Show 15 Items Per Page</option>";
                            }
                            ?>
                        </select>

                    </ul>

                </div>

                <!--  ######################## pagination code block#2 of 2 end ###################################### -->

            </div>

            <!--Messeage-->
            <?php
            echo"
               <div style='margin-top: 5%; margin-left: 40%; position: fixed; text-align: center; z-index: 2'>
                   <span style='background-color: #4cae4c; color: white; border-radius: 4px;' id='message'>$msg</span>
               </div>
               ";
            ?>

            <div class="col-sm-1"></div>
        </div>


    <!--Script Files-->
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);

        function confirm_delete(){

            return confirm("Are You Sure?");

        }

        //Jquery for check empty selection:
        $('#trashMultiple').click(function () {
            if($('.checkbox:checked').length == 0){
                alert("No Item Selected!");
            }
            else{
                var selectionForm = $('#selectionForm');
                selectionForm.submit();
            }
        });

        //Jquery For change the action of the form of this page:
        $('#deleteMultiple').click(function () {
            if($('.checkbox:checked').length == 0){
                alert("No Item Selected!");
            }
            else{
                var r = confirm_delete();
                if(r){
                    var selectionForm = $('#selectionForm');
                    selectionForm.attr("action","delete_Multiple.php");
                    selectionForm.submit();
                }
            }
        });

        //select all checkboxes
        $("#select_all").change(function(){  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function(){ //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function(){ //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length ){
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });

        /********************************************3. Search Suggestion (start)***************************************************/
        $(function() {
            var availableTags = [

                <?php
                echo $comma_separated_keywords; //get data from database using method of control class
                ?>
            ];
            // Filter function to search only from the beginning of the string
            $( "#searchID" ).autocomplete({
                source: function(request, response) {

                    var results = $.ui.autocomplete.filter(availableTags, request.term);

                    results = $.map(availableTags, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });

                    response(results.slice(0, 15));

                }
            });


            $( "#searchID" ).autocomplete({
                select: function(event, ui) {
                    $("#searchID").val(ui.item.label);
                    $("#searchForm").submit();
                }
            });

        });
        /********************************************3. Search Suggestion (end)***************************************************/
    </script>
    </body>
</html>