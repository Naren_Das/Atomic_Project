<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objFavourite = new \App\Favourite\Favourite();

$deleteItems = $_POST['multiple'];

foreach($deleteItems as $deleteId){
    $_GET['id'] = $deleteId;
    $objFavourite->setData($_GET);
    $objFavourite->delete();
}

Utility::redirect($path);