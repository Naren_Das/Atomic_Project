<?php
require_once("../../../vendor/autoload.php");
use App\Favourite\Favourite;
use App\Utility\Utility;

$objFavourite = new Favourite();
$allData = $objFavourite->index();

$tableRows = "";
$sl = 0;

foreach($allData as $row){
    $id = $row->id;
    $customerName = $row->customer_name;
    $favouriteSection = $row->book_list;
    $sl++;
    $tableRows .="<tr>";

    $tableRows .= "<td align='center' width='50'> $sl </td>";
    $tableRows .= "<td align='center' width='50'> $id </td>";
    $tableRows .= "<td align='center' width='50'> $customerName </td>";
    $tableRows .= "<td align='center' width='50'> $favouriteSection </td>";

    $tableRows .= "</tr>";
}

$html =<<<favouriteSection
     <head>
        <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/file.css">
    </head>
    
    <div class="container">
        <p>Customer's Favourite Book Section</p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Serial</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>ID</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Customer Name</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Favourite Section</th>
                </tr>
            </thead>    
            
            <tbody>
                $tableRows;
            </tbody>
        </table>
    </div>
favouriteSection;

//third party class file:
require_once('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

//Output a PDF file directory to the browser: Download file:
$mpdf->Output('Favourite Section.pdf','D');
