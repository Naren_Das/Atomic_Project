<?php
require_once("../../../vendor/autoload.php");
if(!isset($_SESSION))session_start();

use App\Message\Message;
$msg = Message::message();

$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$singleData = $objGender->view();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css">
        <title>Gender View</title>
    </head>
    <body>

        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->

        <div class="col-sm-12 container contentView">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 content">
                <h3>
                    Gender Information - <?php echo $singleData->customer_name ?>
                </h3>
                <hr/>

                <!--Table Design using Bootstrap-->
                <table class="table table-bordered data">
                    <tr>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Gender</th>
                    </tr>

                    <?php

                    echo "
                            <tr>
                                <td>$singleData->id</td>
                                <td>$singleData->customer_name</td>
                                <td>$singleData->customer_gender</td>
                            <tr/>
                        ";

                    ?>

                    <tr>
                        <td colspan="5">
                            <a href="../Gender/index.php"><input type="button" value="Active List" class="btn btn-primary btn-sm"/></a>
                            <a href="../Gender/trashed.php"><input type="button" value="Trash List" class="btn btn-primary btn-sm"/></a>
                        </td>
                    </tr>
                </table>

            </div>

            <!--Messeage-->
            <?php
            echo"
               <div style='margin-top: 5%; margin-left: 40%; position: fixed; text-align: center; z-index: 2'>
                   <span style='background-color: #4cae4c; color: white; border-radius: 4px;' id='message'>$msg</span>
               </div>
               ";
            ?>

            <div class="col-sm-1"></div>
        </div>


    <!--Script Files-->
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);
    </script>
    </body>
</html>