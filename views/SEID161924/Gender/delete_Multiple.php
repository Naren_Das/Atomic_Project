<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objGender = new \App\Gender\Gender();

$deleteItems = $_POST['multiple'];

foreach($deleteItems as $deleteId){
    $_GET['id'] = $deleteId;
    $objGender->setData($_GET);
    $objGender->delete();
}

Utility::redirect($path);