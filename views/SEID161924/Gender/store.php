<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

if(isset($_POST['storeInfo'])){
    $objGender = new \App\Gender\Gender();
    $objGender->setData($_POST);
    $objGender->store();
}

Utility::redirect('create.php');

