<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objGender = new \App\Gender\Gender();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objGender->setData($_GET);
    $objGender->recover();
}

Utility::redirect('index.php');