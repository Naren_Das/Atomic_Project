<?php
require_once("../../../vendor/autoload.php");
if(!isset($_SESSION))session_start();

use App\Message\Message;
$msg = Message::message();

$objGender = new \App\Gender\Gender();
$allData = $objGender->trashed();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css">
        <title>Trashed List</title>
    </head>
    <body>

        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->

        <div class="col-sm-12 container contentView">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 content">
                <h3>Trashed List: Customer Gender</h3>
                <hr/>

                <!--Table Design using Bootstrap-->
                <form id="selectionForm" action="recover_Multiple.php" method="post">
                <table class="table table-bordered data">
                    <tr>
                        <th>Select All <input type="checkbox" id = "select_all" name="select_all"/></th>
                        <th>Serial</th>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>

                    <?php
                    $serial = 1;
                    foreach($allData as $record){
                        echo "
                                        <tr>
                                            <td align='center'><input type='checkbox' class='checkbox' name='multiple[]' value='$record->id'></td>
                                            <td>$serial</td>
                                            <td>$record->id</td>
                                            <td>$record->customer_name</td>
                                            <td>$record->customer_gender</td>
                                            <td>
                                                <a href='../Gender/view.php?id=$record->id'><input type='Button' value='View' class='btn btn-primary btn-sm'></a>
                                                <a href='../Gender/edit.php?id=$record->id'><input type='button' value='Edit' class='btn btn-primary btn-sm'></a>
                                                <a href='../Gender/recover.php?id=$record->id'><input type='button' value='Recover' class='btn btn-success btn-sm'></a>
                                                <a href='../Gender/delete.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger btn-sm'> Delete </a>
                                            </td>
                                        <tr/>
                                    ";
                        $serial++;
                    }
                    ?>

                    <tr>
                        <td colspan="6">
                            <a href="../Gender/create.php"><input type="button" value="Add New" class="btn btn-success btn-sm"/></a>
                            <a href="../Gender/index.php"><input type="button" value="View Active List" class="btn btn-info btn-sm"/></a>
                            <input id="deleteMultiple" type="button" value="Delete Selected" class="btn btn-danger btn-sm"/>
                            <input id="recoverMultiple" type="button" value="Recover Selected" class="btn btn-success btn-sm"/>
                        </td>
                    </tr>
                </table>

            </div>

            <!--Messeage-->
            <?php
            echo"
               <div style='margin-top: 5%; margin-left: 40%; position: fixed; text-align: center; z-index: 2'>
                   <span style='background-color: #4cae4c; color: white; border-radius: 4px;' id='message'>$msg</span>
               </div>
               ";
            ?>

            <div class="col-sm-1"></div>
        </div>


    <!--Script Files-->
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);

        function confirm_delete(){

            return confirm("Are You Sure?");

        }

        //Jquery for check empty selection:
        $('#recoverMultiple').click(function () {
            if($('.checkbox:checked').length == 0){
                alert("No Item Selected!");
            }
            else{
                var selectionForm = $('#selectionForm');
                selectionForm.submit();
            }
        });

        //Jquery For change the action of the form of this page:
        $('#deleteMultiple').click(function () {
            if($('.checkbox:checked').length == 0){
                alert("No Item Selected!");
            }
            else{
                var r = confirm_delete();
                if(r){
                    var selectionForm = $('#selectionForm');
                    selectionForm.attr("action","delete_Multiple.php");
                    selectionForm.submit();
                }
            }
        });

        //select all checkboxes
        $("#select_all").change(function(){  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function(){ //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function(){ //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length ){
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });

    </script>
    </body>
</html>