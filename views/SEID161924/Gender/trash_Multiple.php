<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objGender = new \App\Gender\Gender();

$trashItems = $_POST['multiple'];

foreach($trashItems as $trashId){
    $_GET['id'] = $trashId;
    $objGender->setData($_GET);
    $objGender->trash();
}

Utility::redirect('trashed.php');
