<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objGender = new \App\Gender\Gender();

$objGender->setData($_GET);

$objGender->trash();

Utility::redirect('trashed.php');