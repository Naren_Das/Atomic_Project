<?php
    require_once('../../../vendor/autoload.php');
    use App\Gender\Gender;
    use App\Utility\Utility;

    $objGender = new Gender();
    $allData = $objGender->index();

    $tableRows = "";
    $sl = 0;

foreach ($allData as $row){
    $id = $row->id;
    $customerName = $row->customer_name;
    $customerGender = $row->customer_gender;
    $sl++;
    $tableRows .= "<tr>";

    $tableRows .= "<td align='center' width = '50'> $sl </td>";
    $tableRows .= "<td align='center' width = '50'> $id </td>";
    $tableRows .= "<td align='center' width = '250'> $customerName </td>";
    $tableRows .= "<td align='center' width = '250'> $customerGender </td>";

    $tableRows .= "</tr>";
}

$html =<<<gender
    <head>
        <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/file.css">
    </head>
    
    <div class="container">
        <p>Customer Gender Information</p>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Serial</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>ID</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Customer Name</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Customer Gender</th>
                </tr>
            </thead>
            
            <tbody>
                $tableRows;
            </tbody>
            
        </table>
    </div> 
gender;

//third party class file:
require_once('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

//Output a PDF file directory to the browser: Download file:
$mpdf->Output('CustomerGender.pdf','D');
