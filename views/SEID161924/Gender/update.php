<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objGender = new \App\Gender\Gender();

$objGender->setData($_POST);

$objGender->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}

