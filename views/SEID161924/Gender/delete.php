<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$objGender->delete();

Utility::redirect($path);