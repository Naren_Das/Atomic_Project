<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objCityLocation = new \App\CityLocation\CityLocation();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objCityLocation->setData($_GET);
    $objCityLocation->recover();
}

Utility::redirect('index.php');