<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objCityLocation = new \App\CityLocation\CityLocation();
$objCityLocation->setData($_GET);
$objCityLocation->delete();

Utility::redirect($path);