<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objCityLocation  = new \App\CityLocation\CityLocation();

$objCityLocation->setData($_POST);

$objCityLocation->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}
