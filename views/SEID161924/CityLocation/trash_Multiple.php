<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objCityLocation = new \App\CityLocation\CityLocation();

$trashItems = $_POST['multiple'];

foreach($trashItems as $trashId){
    $_GET['id'] = $trashId;
    $objCityLocation->setData($_GET);
    $objCityLocation->trash();
}

Utility::redirect('trashed.php');
