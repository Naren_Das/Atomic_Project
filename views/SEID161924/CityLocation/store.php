<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

if(isset($_POST['storeInfo'])){
    $objCityLocation = new \App\CityLocation\CityLocation();
    $objCityLocation->setData($_POST);
    $objCityLocation->store();
}

Utility::redirect('create.php');