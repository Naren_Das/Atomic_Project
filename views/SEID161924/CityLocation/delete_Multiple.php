<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objCityLocation = new \App\CityLocation\CityLocation();

$deleteItems = $_POST['multiple'];

foreach($deleteItems as $deleteId){
    $_GET['id'] = $deleteId;
    $objCityLocation->setData($_GET);
    $objCityLocation->delete();
}

Utility::redirect($path);