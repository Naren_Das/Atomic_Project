<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objCityLocation = new \App\CityLocation\CityLocation();

$objCityLocation->setData($_GET);

$objCityLocation->trash();

Utility::redirect('trashed.php');