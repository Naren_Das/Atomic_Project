<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$objProfilePicture->delete();
unlink("Images/".$_GET['image']);

Utility::redirect($path);