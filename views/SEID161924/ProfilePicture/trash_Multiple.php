<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$trashItems = $_POST['multiple'];

foreach($trashItems as $trashId){
    $_GET['id'] = $trashId;
    $objProfilePicture->setData($_GET);
    $objProfilePicture->trash();
}

Utility::redirect('trashed.php');
