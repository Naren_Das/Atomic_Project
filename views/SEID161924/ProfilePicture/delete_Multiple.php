<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$deleteItems = $_POST['multiple'];

foreach($deleteItems as $deleteId){
    $idAndPictureNameArray = explode("#",$deleteId);

    $_GET['id'] = $idAndPictureNameArray[0];
    $objProfilePicture->setData($_GET);
    $objProfilePicture->delete();
    unlink("Images/".$idAndPictureNameArray[1]);
}

Utility::redirect($path);