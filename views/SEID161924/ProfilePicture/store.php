<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;

//file upload work:
$fileName = time().$_FILES['storePicture']['name'];
//file stored in a temporary location when it is uploaded properly
$source = $_FILES['storePicture']['tmp_name'];
$destination = "Images/".$fileName;

//file name transfer to $_POST for database:
$_POST['profilePicture'] = $fileName;

//all data transfer to database using ProfilePicture class via $_POST:
if(move_uploaded_file($source, $destination)){
    $objProfilePicture = new \App\ProfilePicture\ProfilePicture();
    $objProfilePicture->setData($_POST);
    $objProfilePicture->store();
    Utility::redirect('create.php');
}
else{
    Message::message("Error in picture upload!");
    Utility::redirect('create.php');
}
