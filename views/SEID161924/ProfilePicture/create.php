<?php
require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
$msg = Message::message();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="../../../resources/css/style.css">
        <title>Profile Picture</title>
    </head>
    <body>

        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->
        <div class="col-sm-12  container contentView" id="contentDiv">
            <div class="col-sm-4"></div>
            <div class="col-sm-4 content">
                <h3>Set Up Your Profile</h3>
                <hr/>
                <div class="subContent">
                    <form action="store.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="ID">Customer Name: </label>
                            <input type="text" class="form-control" id="ID" name="userName" placeholder="Enter Your Name..." required>
                        </div>

                        <div class="form-group">
                            <label for="Car List">Upload Picture:</label>
                            <input type="file" name="storePicture" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Submit</button>
                            <button type="reset" class="btn btn-default">Refresh</button>
                        </div>

                    </form>

                    <hr/>
                    <a href="index.php" style="float: left"><button class="btn btn-default">View Data</button></a>
                </div>
            </div>

            <!--Messeage-->
            <?php
            echo"
               <div style='margin-top: 5%; margin-left: 40%; position: fixed; text-align: center; z-index: 2'>
                   <span style='background-color: #4cae4c; color: white; border-radius: 4px;' id='message'>$msg</span>
               </div>
               ";
            ?>

            <div class="col-sm-4"></div>
        </div>


    <!--Script Files-->
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery.js"></script>
    <script src="../../../resources/js/jquery-ui.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);

        // For this we have to add an id (Jquery function for slide contentDiv)
       /* $(window).ready(function () {
            $("#contentDiv").hide();
            $("#contentDiv").slideDown(2000);
        });*/
    </script>
    </body>
</html>