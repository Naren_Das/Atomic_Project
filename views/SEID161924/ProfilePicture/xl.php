<?php
require_once('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;
$objProfilePicture = new ProfilePicture();
$allData = $objProfilePicture->index();

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once("../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");

//create new PHPExcel object:
$objPHPExcel = new PHPExcel();

//set document properties:
$objPHPExcel->getProperties()->setCreator("Naren Das")
    ->setLastModifiedBy("Naren Das")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

//Add some data:
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1','Serial')
    ->setCellValue('B1','ID')
    ->setCellValue('C1','Name')
    ->setCellValue('D1','Profile Picture');

//Add some style to the header Row:
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setItalic(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(12);

$sl = 0;
$counter = 1;
foreach ($allData as $row){
    $sl++;
    $counter++;

    //Multiple data read from database:
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $sl)
        ->setCellValue('B'.$counter, $row->id)
        ->setCellValue('C'.$counter, $row->name);
    $objPHPExcel->getActiveSheet()->getRowDimension($counter)->setRowHeight(80);

    $image = imagecreatefromjpeg('Images/'.$row->profile_picture);

    //Add image to the Excel Sheet from the databases:
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample Image');
    $objDrawing->setDescription('Sample Image');
    $objDrawing->setImageResource($image);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setHeight(160);
    $objDrawing->setWidth(110);
    $objDrawing->setCoordinates('D'.$counter);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

    //Rename Worksheet:
    $objPHPExcel->getActiveSheet()->setTitle('Customer Picture');
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Profile Picture.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter=save('PP.xlsx');
ob_end_clean();// For clear output buffering
$objWriter->save('php://output');

exit;
