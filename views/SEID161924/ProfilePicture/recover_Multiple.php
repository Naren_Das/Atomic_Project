<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objProfilePicture->setData($_GET);
    $objProfilePicture->recover();
}

Utility::redirect('index.php');