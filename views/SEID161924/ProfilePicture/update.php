<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;

//file upload work:
$fileName = time().$_FILES['newPicture']['name'];
//file stored in a temporary locaiton when it is uploaded properly:
$source = $_FILES['newPicture']['tmp_name'];
$destination = "Images/".$fileName;

//file name transfer to $_POST for database:
$_POST['profilePicture'] = $fileName;

//all data update in database using ProfilePicture class via $_POST
if(move_uploaded_file($source,$destination)){
    $objProfilePicture = new \App\ProfilePicture\ProfilePicture();
    unlink("Images/".$_POST['oldImage']);
    $objProfilePicture->setData($_POST);
    $objProfilePicture->update();

    if($_POST['mode'] == 1){
        Utility::redirect('index.php');
    }
    else{
        Utility::redirect('trashed.php');
    }
}
else{
    Message::message("Error in new picture upload!");
    Utility::redirect('edit.php');
}
