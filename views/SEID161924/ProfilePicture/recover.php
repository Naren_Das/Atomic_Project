<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$objProfilePicture->setData($_GET);

$objProfilePicture->recover();

Utility::redirect('index.php');