<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$objBookSummary->setData($_POST);

$objBookSummary->update();

if($_POST['mode'] == 1){
    Utility::redirect('index.php');
}
else{
    Utility::redirect('trashed.php');
}
