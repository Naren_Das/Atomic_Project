<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();
$objBookSummary->setData($_GET);
$objBookSummary->delete();

Utility::redirect($path);
