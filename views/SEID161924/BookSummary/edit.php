<?php
require_once("../../../vendor/autoload.php");
// for get data from the database to the input field for update:
$objBookSummary = new \App\BookSummary\BookSummary();
$objBookSummary->setData($_GET);
$singleData = $objBookSummary->view();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!--Stylesheet Files-->
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/style.css">
        <title>Update Information</title>
    </head>
    <body>
        <!--Header Section-->
        <div class="header navbar-fixed-top">
            <!--Logo-->
            <div class="logo">
                <img src="../../../resources/images/Project_logo.png"/>
            </div>

            <!-- Header Section: Navmenu-->
            <div class="navbar">
                <div class="container">
                    <div class="navMenu font">
                        <ul>
                            <li><a href="../../../views/SEID161924/index.html">Home</a></li>
                            <li><a href="../../../views/SEID161924/BookTitle/create.php">Book's Information</a></li>
                            <li><a href="../../../views/SEID161924/BookSummary/create.php">Summary</a></li>
                            <li><a href="../../../views/SEID161924/Favourite/create.php">Add Favourite</a></li>
                            <li><a href="../../../views/SEID161924/Gender/create.php">Gender</a></li>
                            <li><a href="../../../views/SEID161924/CityLocation/create.php">City Location</a></li>
                            <li><a href="../../../views/SEID161924/Birthdate/create.php">Birthday</a></li>
                            <li><a href="../../../views/SEID161924/ProfilePicture/create.php">Portfolio Picture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Content Section-->

        <div class="col-sm-12 container contentView">
            <div class="col-sm-4"></div>
            <div class="col-sm-4 content">
                <h3>Update Your Information</h3>
                <hr/>
                <div class="subContent">
                    <form action="../BookSummary/update.php" method="post">
                        <div class="form-group">
                            <label for="ID">Book Title: </label>
                            <input type="text" class="form-control" id="ID" name="bookName" value='<?php echo $singleData->book_title?>' required>
                        </div>

                        <div class="form-group">
                            <label for="Name">Details Summery: </label>
                            <textarea class="form-control" name="comment" rows="6" cols="20" required><?php echo $singleData->details_summery?></textarea>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

                        <input type="hidden" name="mode" value="<?php echo $_GET['mode']?>">

                        <button type="submit" name="storeData" class="btn btn-default">Update</button>
                        <button type="reset" class="btn btn-default">Refresh</button>
                    </form>
                </div>
            </div>

            <div class="col-sm-4"></div>
        </div>

    <!--Script Files-->
    <script src="../../../resources/js/app.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/js/jquery-3.2.1.min.js"></script>
    <script>
        //Jquery For Message Span Animation
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeToggle(550);
        $('#message').fadeOut(550);

        //Jquery for container div slide Note: For this we have to add an id
        /*$(window).ready(function(){
         $("#contentDiv").hide();
         $("#contentDiv").slideDown(2000);
         });*/
    </script>
    </body>
</html>