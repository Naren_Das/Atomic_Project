<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$trashItems = $_POST['multiple'];

foreach ($trashItems as $itemId){
    $_GET['id'] = $itemId;
    $objBookSummary->setData($_GET);
    $objBookSummary->trash();
}

Utility::redirect('trashed.php');