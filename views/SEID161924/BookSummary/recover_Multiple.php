<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$recoverItems = $_POST['multiple'];

foreach ($recoverItems as $itemId){
    $_GET['id'] = $itemId;
    $objBookSummary->setData($_GET);
    $objBookSummary->recover();
}

Utility::redirect('index.php');