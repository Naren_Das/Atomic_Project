<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$objBookSummary->setData($_GET);

$objBookSummary->trash();

Utility::redirect('trashed.php');