<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$objBookSummary->setData($_GET);

$objBookSummary->recover();

Utility::redirect('index.php');