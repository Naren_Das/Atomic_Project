<?php
    require_once ("../../../vendor/autoload.php");

    use App\Utility\Utility;

    if(isset($_POST['storeData'])){
        $objBookSummery = new \App\BookSummary\BookSummary();
        $objBookSummery->setData($_POST);
        $objBookSummery->store();
    }

    Utility::redirect('create.php');