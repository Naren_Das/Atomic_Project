<?php
$path = $_SERVER['HTTP_REFERER'];
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

$objBookSummary = new \App\BookSummary\BookSummary();

$delteItems = $_POST['multiple'];

foreach ($delteItems as $itemId){
    $_GET['id'] = $itemId;
    $objBookSummary->setData($_GET);
    $objBookSummary->delete();
}

Utility::redirect($path);