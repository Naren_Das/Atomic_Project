<?php
require_once("../../../vendor/autoload.php");
use App\BookSummary\BookSummary;
use App\Utility\Utility;

$objBookSummary = new BookSummary();
$allData = $objBookSummary->index();

$tableRows = "";
$sl = 0;

foreach ($allData as $row){
    $id = $row->id;
    $bookName = $row->book_title;
    $detailsSummary = $row->details_summery;
    $sl++;

    $tableRows .= "<tr>";

    $tableRows .= "<td align='center' width='50'> $sl </td>";
    $tableRows .= "<td align='center' width='50'> $id </td>";
    $tableRows .= "<td align='center' width='250'> $bookName </td>";
    $tableRows .= "<td align='center' width='250'> $detailsSummary </td>";

    $tableRows .= "</tr>";
}

$html =<<<bookSummery
    <head>
        <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/file.css">
    </head>

    <div class="container">
        <p>Book's Summary</p>
    </div>
    
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Serial</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>ID</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Book Name</th>
                    <th align='center' style='background-color: #4c4d4d; color: gainsboro;'>Details Summary</th>
                </tr>
            </thead>
            
            <tbody>
                $tableRows;
            </tbody>
        </table>
    </div>
bookSummery;

//third party class file:
require_once('../../../vendor/mpdf/mpdf/mpdf.php');

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

//Output a PDF file directory to the browser: Download file
$mpdf->Output('BookSummary.pdf','D');