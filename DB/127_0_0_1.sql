-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2017 at 07:05 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_161924`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_161924` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_161924`;

-- --------------------------------------------------------

--
-- Table structure for table `book_summery`
--

CREATE TABLE `book_summery` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `details_summery` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_summery`
--

INSERT INTO `book_summery` (`id`, `book_title`, `details_summery`, `is_trashed`) VALUES
(3, 'Data Structure', 'It is the best book for data structure. ', 'no'),
(7, 'Programming with C ', 'Best book for programming.', 'no'),
(8, 'Programming With C++', 'Best book for object oriented programming.', 'no'),
(9, 'Programming With Java', 'Herbert Sheild', 'no'),
(10, 'CSS 3 ', 'Md. Mizanur Rahman', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(14, 'Programming With C', 'E. Balagurushamy', 'no'),
(21, '  Computer Graphics', 'Elmond morich', 'no'),
(34, 'Satkahan', 'Samaresh Majumdar', 'no'),
(35, 'Programming With C++', 'E. Balagurushamy', 'no'),
(36, 'Programming with Java.', 'Herbert Sheild', 'no'),
(37, 'Amar Bondhu Rashed', 'Sattajit Roy', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `customer_birthdate`
--

CREATE TABLE `customer_birthdate` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_birthdate`
--

INSERT INTO `customer_birthdate` (`id`, `customer_name`, `birthdate`, `is_trashed`) VALUES
(11, 'Naren Das', '1993-12-02', 'no'),
(12, 'Subrata Nath', '2017-06-02', 'no'),
(13, 'Pias shil', '2017-06-14', 'no'),
(14, 'Anika Dutta', '2017-06-21', 'no'),
(16, 'Sajib Dhar', '2017-06-28', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `customer_choice`
--

CREATE TABLE `customer_choice` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `book_list` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_choice`
--

INSERT INTO `customer_choice` (`id`, `customer_name`, `book_list`, `is_trashed`) VALUES
(12, 'Naren Das', 'Science Fiction,Romance,Horror', 'no'),
(13, 'Subrata nath', 'Science Fiction,Horror', 'no'),
(14, 'Pias Shil', 'Science Fiction,Drama', 'no'),
(15, 'Sajib Dhar', 'Science Fiction,Romance', 'no'),
(16, 'Anika Dutta', 'Drama,Romance', 'no'),
(17, 'Roman Das', 'Drama,Romance,Horror', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `customer_gender`
--

CREATE TABLE `customer_gender` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_gender`
--

INSERT INTO `customer_gender` (`id`, `customer_name`, `customer_gender`, `is_trashed`) VALUES
(3, 'Naren Das ', 'Male', 'no'),
(4, 'Srabon Pias', 'Male', 'no'),
(5, 'Maria Jhonson', 'Female', 'no'),
(6, 'Sajib Dhar', 'Male', 'no'),
(7, 'Anika Dutta', 'Female', 'no'),
(8, 'Subrata Nath', 'Male', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `customer_location`
--

CREATE TABLE `customer_location` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_location`
--

INSERT INTO `customer_location` (`id`, `customer_name`, `customer_city`, `is_trashed`) VALUES
(2, 'Naren Das', 'Dhaka', 'no'),
(3, 'Subrata Nath', 'Dhaka', 'no'),
(4, 'Maria', 'Chittagong', 'no'),
(5, 'Jhon mark', 'Rangpur', 'no'),
(6, 'Jony Chowdhury', 'Chittagong', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `profile_details`
--

CREATE TABLE `profile_details` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_details`
--

INSERT INTO `profile_details` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(2, 'Steve Jobs  ', '1497276390814360.jpg', 'no'),
(4, 'Albert Einstein', '1496765307Wall.jpg', 'no'),
(11, 'Naren Das    ', '1497149552up.jpg', 'no'),
(12, 'Mujibur Rahman', '1497848507Mujib.jpg', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_summery`
--
ALTER TABLE `book_summery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_birthdate`
--
ALTER TABLE `customer_birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_choice`
--
ALTER TABLE `customer_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_gender`
--
ALTER TABLE `customer_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_location`
--
ALTER TABLE `customer_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_details`
--
ALTER TABLE `profile_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_summery`
--
ALTER TABLE `book_summery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `customer_birthdate`
--
ALTER TABLE `customer_birthdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `customer_choice`
--
ALTER TABLE `customer_choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `customer_gender`
--
ALTER TABLE `customer_gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `customer_location`
--
ALTER TABLE `customer_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profile_details`
--
ALTER TABLE `profile_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
