<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-06-17
 * Time: 12.38
 */

namespace App\ProfilePicture;

use App\Utility\Utility;
use PDO;
use App\Model\Database;
use App\Message\Message;

class ProfilePicture extends Database{
    public $id;
    public $name;
    public $pictureName;

    public function setData($receieveDataArray){

        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray['id'];
        }

        if(array_key_exists("userName",$receieveDataArray)){
            $this->name = $receieveDataArray['userName'];
        }

        if(array_key_exists("profilePicture",$receieveDataArray)){
            $this->pictureName = $receieveDataArray['profilePicture'];
        }

    }//end of setData()

    public function store(){
        $name = $this->name;
        $profilePicture = $this->pictureName;
        $dataArray = array($name,$profilePicture);

        $sqlQuery = "INSERT INTO `profile_details` (`name`, `profile_picture`) VALUES (?, ?);";

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted sucessfully!");
        }
        else{
            Message::message("Error in data insertion!");
        }
    }//end of store()

    public function index(){

        $sqlQuery = "select * from profile_details where is_trashed = 'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of index()

    public function view(){
        $sqlQuery = "select * from profile_details where id = ".$this->id;

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;
    }//end of view()

    public function update(){

        $name = $this->name;
        $profilePicture = $this->pictureName;
        $dataArray = array($name,$profilePicture);

        $sqlQuery = "UPDATE `profile_details` SET `name` = ?, `profile_picture` = ? WHERE `id` = $this->id;";

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Picture has been changed sucessfully!");
        }
        else{
            Message::message("Error in picture change!");
        }

    }//end of update()

    public function trash(){
        $sqlQuery = "UPDATE profile_details SET is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been trashed successfully!");
        }
        else{
            Message::message("Error in data trash!");
        }
    }//end of trash()

    public function trashed(){
        $sqlQuery = "select * from profile_details where is_trashed <>'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of trashed()

    public function recover(){

        $sqlQuery = "UPDATE profile_details SET is_trashed = 'no' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been recovered successfully!");
        }
        else{
            Message::message("Error in data recover!");
        }

    }//end of recover()

    public function delete(){

        $sqlQuery = "DELETE from profile_details WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been deleted successfully!");
        }
        else{
            Message::message("Error in data delete!");
        }

    }//end of delete()

    public function search($requestArray){
        $sql = "";

        if (isset($requestArray['byName']) || !isset($requestArray['byName'])){
            $sql = "select * from profile_details WHERE is_trashed = 'no' AND name LIKE '%".$requestArray['search']."%'";
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//end of search()

    public  function getAllKeywords(){

        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData){
            $_allKeywords[] = trim($oneData->name);
        }

        foreach ($allData as $oneData){

            $eachString= strip_tags($oneData->name);
            $eachString=trim($eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }

        return array_unique($_allKeywords);

    }//end of getAllKeywords()

    public function indexPaginator($page = 1, $itemPerPage = 3){
        $start = (($page-1) * $itemPerPage);

        if($start < 0){
            $start =  0;
        }

        $sql = "select * from profile_details where is_trashed = 'no' LIMIT $start, $itemPerPage";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $someDataArray = $STH->fetchAll();
        return $someDataArray;

    }//end of indexPaginator()


}//end of ProfilePicture class