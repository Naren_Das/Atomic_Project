<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/30/2017
 * Time: 11:10 PM
 */

namespace App\BookTitle;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class BookTitle extends Database{
    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($receieveDataArray){

        //here id further use for retrive data from database:
        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray['id'];
        }

        if(array_key_exists("bookName",$receieveDataArray)){
            $this->bookTitle = $receieveDataArray['bookName'];
        }

        if(array_key_exists("authorName",$receieveDataArray)){
            $this->authorName = $receieveDataArray['authorName'];
        }

    }//end of setData()


    public function store(){
        $bookTitle = $this->bookTitle;
        $authorName = $this->authorName;

        $sqlQuery = "INSERT INTO `book_title` ( `book_title`, `author_name`) VALUES (?, ?);";
        $dataArray = array($bookTitle, $authorName);

        $STH = $this->DBH->prepare($sqlQuery); //for data insert into database we have to use prepare//
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted successfully!");
        }
        else{
            Message::message("Error in data insertion!");
        }
    }//end of store()

    public function index(){
        $sqlQuery = "select * from book_title where is_trashed = 'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of index()

    public function view(){
        $sqlQuery = "select * from book_title where id = $this->id";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;
    }//end of view()

    public function update(){
        $bookTitle = $this->bookTitle;
        $authorName = $this->authorName;

        $sqlQuery = "UPDATE `book_title` SET `book_title` = ?, `author_name` = ? WHERE `book_title`.`id` = $this->id;";
        $dataArray = array($bookTitle, $authorName);

        $STH = $this->DBH->prepare($sqlQuery); //for data insert into database we have to use prepare//
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been updated successfully!");
        }
        else{
            Message::message("Error in data update!");
        }
    }//end of update()

    public function trash(){
        $sqlQuery = "UPDATE book_title SET 	is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been trashed successfully!");
        }
        else{
            Message::message("Error in data trash!");
        }
    }//end of trash()

    public function trashed(){
        $sqlQuery = "select * from book_title where is_trashed <>'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of trashed()

    public function recover(){

        $sqlQuery = "UPDATE book_title SET 	is_trashed = 'no' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been recovered successfully!");
        }
        else{
            Message::message("Error in data recover!");
        }

    }//end of recover()

    public function delete(){

        $sqlQuery = "DELETE from book_title WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been deleted successfully!");
        }
        else{
            Message::message("Error in data delete!");
        }

    }//end of delete()

    public function search($requestArray){
        $sql = "";

        if(isset($requestArray['byTitle']) && isset($requestArray['byAuthor'])){
            $sql = "select * from book_title where is_trashed = 'no' AND (book_title LIKE '%".$requestArray['search']."%' OR author_name LIKE '%".$requestArray['search']."%')";
        }
        elseif (isset($requestArray['byTitle']) && !isset($requestArray['byAuthor'])){
            $sql = "select * from book_title WHERE is_trashed = 'no' AND book_title LIKE '%".$requestArray['search']."%'";
        }
        elseif (!isset($requestArray['byTitle']) && isset($requestArray['byAuthor'])){
            $sql = "select * from book_title WHERE is_trashed = 'no' AND author_name LIKE '%".$requestArray['search']."%'";
        }

        elseif (!isset($requestArray['byTitle']) && !isset($requestArray['byAuthor'])){
            $sql = "select * from book_title where is_trashed = 'no' AND (book_title LIKE '%".$requestArray['search']."%' OR author_name LIKE '%".$requestArray['search']."%')";
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

     }//end of search()

    public  function getAllKeywords(){

            $_allKeywords = array();
            $WordsArr = array();
            $allData = $this->index();

            foreach ($allData as $oneData){
                $_allKeywords[] = trim($oneData->book_title);
            }

            foreach ($allData as $oneData){

                $eachString= strip_tags($oneData->book_title);
                $eachString=trim($eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);

                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }


            $allData = $this->index();

            foreach ($allData as $oneData) {
                $_allKeywords[] = trim($oneData->author_name);
            }

            foreach ($allData as $oneData) {

                $eachString= strip_tags($oneData->author_name);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);
                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }

        return array_unique($_allKeywords);

    }//end of getAllKeywords()

    public function indexPaginator($page = 1, $itemPerPage = 3){
        $start = (($page-1) * $itemPerPage);

        if($start < 0){
            $start =  0;
        }

        $sql = "select * from book_title where is_trashed = 'no' LIMIT $start, $itemPerPage";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $someDataArray = $STH->fetchAll();
        return $someDataArray;

    }//end of indexPaginator()

}//end of BookTitle Class