<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/8/2017
 * Time: 9:39 AM
 */

namespace App\Birthdate;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class Birthdate extends Database{
    public $id;
    public $customerName;
    public $customerBirthdate;

    public function setData($receieveDataArray){
        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray['id'];
        }

        if(array_key_exists("customerName",$receieveDataArray)){
            $this->customerName =  $receieveDataArray['customerName'];
        }

        if(array_key_exists("birthDate",$receieveDataArray)){
            $this->customerBirthdate = $receieveDataArray['birthDate'];
        }
    }//end of setData()

    public function store(){

        $cName = $this->customerName;
        $cBirthdate = $this->customerBirthdate;

        $sqlQuery = "INSERT INTO `customer_birthdate` (`customer_name`, `birthdate`) VALUES (?,?);";
        $dataArray = array($cName,$cBirthdate);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted successfully!");
        }
        else{
            Message::message("Error in data insertion!");
        }
    }//end of store()

    public function index(){

        $sqlQuery = "SELECT * FROM `customer_birthdate` where is_trashed = 'no'";

        $STH = $this->DBH->query($sqlQuery);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }//end of index()

    public function view(){

        $sqlQuery = "select * from customer_birthdate where id = ".$this->id;

        $STH = $this->DBH->query($sqlQuery);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;
    }//end of view()

    public function update(){

        $cName = $this->customerName;
        $cBirthdate = $this->customerBirthdate;

        $sqlQuery = "UPDATE `customer_birthdate` SET `customer_name` = ?, `birthdate` = ? WHERE `customer_birthdate`.`id` = $this->id;";
        $dataArray = array($cName,$cBirthdate);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been updated successfully!");
        }
        else{
            Message::message("Error in data update!");
        }

    }//end of update()

    public function trash(){
        $sqlQuery = "UPDATE customer_birthdate SET is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been trashed successfully!");
        }
        else{
            Message::message("Error in data trash!");
        }
    }//end of trash()

    public function trashed(){
        $sqlQuery = "select * from customer_birthdate where is_trashed <>'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of trashed()

    public function recover(){

        $sqlQuery = "UPDATE customer_birthdate SET 	is_trashed = 'no' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been recovered successfully!");
        }
        else{
            Message::message("Error in data recover!");
        }

    }//end of recover()

    public function delete(){

        $sqlQuery = "DELETE from customer_birthdate WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been deleted successfully!");
        }
        else{
            Message::message("Error in data delete!");
        }

    }//end of delete()


    public function search($requestArray){
        $sql = "";

        if(isset($requestArray['byName']) && isset($requestArray['byDate'])){
            $sql = "select * from customer_birthdate where is_trashed = 'no' AND (customer_name LIKE '%".$requestArray['search']."%' OR birthdate LIKE '%".$requestArray['search']."%')";
        }
        elseif (isset($requestArray['byName']) && !isset($requestArray['byDate'])){
            $sql = "select * from customer_birthdate WHERE is_trashed = 'no' AND customer_name LIKE '%".$requestArray['search']."%'";
        }
        elseif (!isset($requestArray['byName']) && isset($requestArray['byDate'])){
            $sql = "select * from customer_birthdate WHERE is_trashed = 'no' AND birthdate LIKE '%".$requestArray['search']."%'";
        }

        elseif (!isset($requestArray['byName']) && !isset($requestArray['byDate'])){
            $sql = "select * from customer_birthdate where is_trashed = 'no' AND (customer_name LIKE '%".$requestArray['search']."%' OR birthdate LIKE '%".$requestArray['search']."%')";
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//end of search()

    public  function getAllKeywords(){

        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData){
            $_allKeywords[] = trim($oneData->customer_name);
        }

        foreach ($allData as $oneData){

            $eachString= strip_tags($oneData->customer_name);
            $eachString=trim($eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }


        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->birthdate);
        }

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->birthdate);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }

        return array_unique($_allKeywords);

    }//end of getAllKeywords()

    public function indexPaginator($page = 1, $itemPerPage = 3){
        $start = (($page-1) * $itemPerPage);

        if($start < 0){
            $start =  0;
        }

        $sql = "select * from customer_birthdate where is_trashed = 'no' LIMIT $start, $itemPerPage";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $someDataArray = $STH->fetchAll();
        return $someDataArray;

    }//end of indexPaginator()

}//end of Birthdate class