<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-06-17
 * Time: 08.24
 */

namespace App\Favourite;

use App\Utility\Utility;
use PDO;
use App\Model\Database;
use App\Message\Message;

class Favourite extends Database{
    public $id;
    public $customerName;
    public $bookName;

    public function setData($receieveDataArray){

        //here id further use for retrive data from database:
        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray['id'];
        }

        if(array_key_exists("customerName",$receieveDataArray)){
            $this->customerName = $receieveDataArray['customerName'];
        }

        if(array_key_exists("book",$receieveDataArray)){
            //Adding each value of checkbox in an string type variable:
            $bookArray = $receieveDataArray['book'];
            $chooseString = implode(",",$bookArray);
            $this->bookName = $chooseString;
        }

    }//end of setData()


    public function store(){
        $cName = $this->customerName;
        $bookList = $this->bookName;

        $sqlQuery = "INSERT INTO `customer_choice` ( `customer_name`, `book_list`) VALUES (?, ?);";
        $dataArray = array($cName, $bookList);

        $STH = $this->DBH->prepare($sqlQuery); //As like as set data use for insert data
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted successfully!");
        }
        else{
            Message::message("Error in data insertion!");
        }
    }//end of store()

    public function index(){

        $sqlQuery = "select * from customer_choice where is_trashed = 'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of index()


    public function view(){
        $sqlQuery = "select * from customer_choice where id = ".$this->id;

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;
    }//end of view()

    public function update(){
        $cName = $this->customerName;
        $bookList = $this->bookName;

        $sqlQuery = "UPDATE `customer_choice` SET `customer_name` = ?, `book_list` = ? WHERE `customer_choice`.`id` = $this->id;";
        $dataArray = array($cName, $bookList);

        $STH = $this->DBH->prepare($sqlQuery); //As like as set data use for insert data
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been updated successfully!");
        }
        else{
            Message::message("Error in data update!");
        }
    }//end of update()

    public function trash(){

        $sqlQuery = "UPDATE customer_choice SET is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been trashed successfully!");
        }
        else{
            Message::message("Error in data trash!");
        }

    }//end of trash();

    public function trashed(){

        $sqlQuery = "select * from customer_choice where is_trashed <>'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;

    }//end of trashed()

    public function recover(){

        $sqlQuery = "UPDATE customer_choice SET is_trashed = 'no' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been recovered successfully!");
        }
        else{
            Message::message("Error in data recover!");
        }

    }//end of recover()

    public function delete(){

        $sqlQuery = "DELETE from customer_choice WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been deleted successfully!");
        }
        else{
            Message::message("Error in data delete!");
        }

    }//end of delete()

    public function search($requestArray){
        $sql = "";

        if(isset($requestArray['byName']) && isset($requestArray['bySection'])){
            $sql = "select * from customer_choice where is_trashed = 'no' AND (customer_name LIKE '%".$requestArray['search']."%' OR book_list LIKE '%".$requestArray['search']."%')";
        }
        elseif (isset($requestArray['byName']) && !isset($requestArray['bySection'])){
            $sql = "select * from customer_choice WHERE is_trashed = 'no' AND customer_name LIKE '%".$requestArray['search']."%'";
        }
        elseif (!isset($requestArray['byName']) && isset($requestArray['bySection'])){
            $sql = "select * from customer_choice WHERE is_trashed = 'no' AND book_list LIKE '%".$requestArray['search']."%'";
        }

        elseif (!isset($requestArray['byName']) && !isset($requestArray['bySection'])){
            $sql = "select * from customer_choice where is_trashed = 'no' AND (customer_name LIKE '%".$requestArray['search']."%' OR book_list LIKE '%".$requestArray['search']."%')";
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//end of search()


    public  function getAllKeywords(){

        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData){
            $_allKeywords[] = trim($oneData->customer_name);
        }

        foreach ($allData as $oneData){

            $eachString= strip_tags($oneData->customer_name);
            $eachString=trim($eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }


        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_list);
        }

        foreach ($allData as $oneData){
            $eachString= strip_tags($oneData->book_list);
            $eachString=trim( $eachString);

            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString = str_replace(","," ",$eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }

        return array_unique($_allKeywords);

    }//end of getAllKeywords()

    public function indexPaginator($page = 1, $itemPerPage = 3){
        $start = (($page-1) * $itemPerPage);

        if($start < 0){
            $start =  0;
        }

        $sql = "select * from customer_choice where is_trashed = 'no' LIMIT $start, $itemPerPage";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $someDataArray = $STH->fetchAll();
        return $someDataArray;

    }//end of indexPaginator()
}