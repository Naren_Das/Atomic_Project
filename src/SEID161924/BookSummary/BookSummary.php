<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/31/2017
 * Time: 12:30 PM
 */

namespace App\BookSummary;

use PDO;
use App\Message\Message;
use App\Model\Database;

class BookSummary extends Database{
    public $id;
    public $bookTitle;
    public $bookOpinion;

    public function setData($receieveDataArray){

        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray["id"];
        }

        if(array_key_exists("bookName",$receieveDataArray)){
            $this->bookTitle = $receieveDataArray["bookName"];
        }

        if(array_key_exists("comment",$receieveDataArray)){
            $this->bookOpinion = $receieveDataArray["comment"];
        }
    }//end of SetData()

    public function store(){
        $bookName = $this->bookTitle;
        $detailsSummery = $this->bookOpinion;

        $sqlQuery = "INSERT INTO `book_summery` (`book_title`,`details_summery`) VALUES (?, ?);";
        $dataArray = array($bookName,$detailsSummery);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been inserted successfully!");
        }
        else{
            Message::message("Error in Data insertion!");
        }

    }//end of store()

    public function index(){
        $sqlQuery = "select * from book_summery where is_trashed = 'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;
    }//end of index()

    public function view(){
        $sqlQuery = "select * from book_summery where id = $this->id";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;
    }

    public function update(){

        $bookName = $this->bookTitle;
        $detailsSummery = $this->bookOpinion;

        $sqlQuery = "UPDATE `book_summery` SET `book_title` = ?, `details_summery` = ? WHERE `book_summery`.`id` = $this->id;";
        $dataArray = array($bookName,$detailsSummery);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Data has been updated successfully!");
        }
        else{
            Message::message("Error in Data update!");
        }

    }//end of update()

    public function trash(){

        $sqlQuery = "UPDATE book_summery SET is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been trashed successfully!");
        }
        else{
            Message::message("Error in data trash!");
        }

    }//end of trash()

    public function trashed(){

        $sqlQuery = "select * from book_summery where is_trashed <>'no'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData = $STH->fetchAll();

        return $allData;

    }//end of trashed()

    public function recover(){

        $sqlQuery = "UPDATE book_summery SET is_trashed = 'no' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been recovered successfully!");
        }
        else{
            Message::message("Error in data recover!");
        }

    }//end of recover()

    public function delete(){

        $sqlQuery = "DELETE from book_summery WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Data has been deleted successfully!");
        }
        else{
            Message::message("Error in data delete!");
        }

    }//end of delete()

    public function search($requestArray){
        $sql = "";

        if(isset($requestArray['byTitle']) && isset($requestArray['bySummary'])){
            $sql = "select * from book_summery where is_trashed = 'no' AND (book_title LIKE '%".$requestArray['search']."%' OR details_summery LIKE '%".$requestArray['search']."%')";
        }
        elseif (isset($requestArray['byTitle']) && !isset($requestArray['bySummary'])){
            $sql = "select * from book_summery WHERE is_trashed = 'no' AND book_title LIKE '%".$requestArray['search']."%'";
        }
        elseif (!isset($requestArray['byTitle']) && isset($requestArray['bySummary'])){
            $sql = "select * from book_summery WHERE is_trashed = 'no' AND details_summery LIKE '%".$requestArray['search']."%'";
        }

        elseif (!isset($requestArray['byTitle']) && !isset($requestArray['bySummary'])){
            $sql = "select * from book_summery where is_trashed = 'no' AND (book_title LIKE '%".$requestArray['search']."%' OR details_summery LIKE '%".$requestArray['search']."%')";
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }//end of search()

    public  function getAllKeywords(){

        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData){
            $_allKeywords[] = trim($oneData->book_title);
        }

        foreach ($allData as $oneData){

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim($eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }


        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->details_summery);
        }

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->details_summery);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }

        return array_unique($_allKeywords);

    }//end of getAllKeywords()

    public function indexPaginator($page = 1, $itemPerPage = 3){
        $start = (($page-1) * $itemPerPage);

        if($start < 0){
            $start =  0;
        }

        $sql = "select * from book_summery where is_trashed = 'no' LIMIT $start, $itemPerPage";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $someDataArray = $STH->fetchAll();
        return $someDataArray;

    }//end of indexPaginator()

}//end of BookSummary class