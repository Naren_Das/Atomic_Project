<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/30/2017
 * Time: 11:43 PM
 */

namespace App\Model;

use PDO,PDOException;

class Database{
    public $DBH;

    public function __construct(){

        try{
            $this->DBH =  new PDO("mysql:host=localhost;dbname=atomic_project_161924", "root", "");
            //this is use for show exceptional error occurance and hide the important data
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $error){
            echo $error->getMessage();
        }

    }
}